 $(document).ready(function(){

  $('.ml-loader').hide();
  
    var x = document.getElementById("landingButtons");
  	x.style.display = "block";
});


function documentSamplesClick(){
  window.location="../views/documentSamples.html";
}

function viewDemoClick(){
  window.location="../views/unstructured.html";
}

function structuredClick(){
  window.location="../views/structured.html";
}

function semiStructuredClick(){
  window.location="../views/semiStructured.html";
}

function unStructuredClick(){
  window.location="../views/unstructured.html";
}

function imagesClick(){
  window.location="../views/images.html";
}

function restartClick(){
  window.location="../index.html";
}

function autoplayClick(){
  window.location="../views/unstructured.html?autoPlay=true";
}

function closeClick(){
  window.location="../index.html?close=true";
}
