const queryString = window.location.search;

const urlParams = new URLSearchParams(queryString);
const autoPlay = urlParams.get('autoPlay');

 $(document).ready(function(){

  $('.ml-loader').hide();
  //  console.log('Ready');

    if(!autoPlay)
      return;

    var imagesVideo = document.getElementById("images");
    imagesVideo.onended = function(){ onVideoEnd();}


});


function onVideoEnd(){
  console.log('onVideoEnd');
    window.location="../views/unstructured.html?autoPlay=true";
}
