//  enum State {
//   Start = 0,
//   DirectAccept,
//   Modify,
//   ModifyAccept,
//   DirectReversed,
//   ModifyReversed
// }

 const queryString = window.location.search;

 const urlParams = new URLSearchParams(queryString);
 const autoPlay = urlParams.get('autoPlay');


// let theState = State.Start;
let theState = 0; //0-Start, 1-DirectAccept, 2-Modify, 3-ModifyAccept

 $(document).ready(function(){

  $('.ml-loader').hide();
  //  console.log('Ready');
  
   // var x = document.getElementById("landingButtons");
   // x.style.display = "block";
    var popUp = document.getElementById("popUp");
    popUp.style.display = "none";
    setTimeout(function() { popUp.style.display = "block"; }, 2000);
    popUp.onended = function() {
      console.log("PopUp video has ended");
      popUp.style.display = "none";
    };

    var errorIcon = document.getElementById("errorIcon");
    errorIcon.style.display = "none";

    var semiStructuredButtons = document.getElementById("semiStructuredButtons");
    semiStructuredButtons.style.display = "none";

    // theState = State.Start;
    theState = 0;

    var semiStructuredVideo = document.getElementById("semiStructured");
    semiStructuredVideo.onended = function(){ onStartVideoEnd();}

});


function onStartVideoEnd(){
  console.log('onStartVideoEnd');
    var errorIcon = document.getElementById("errorIcon");
    errorIcon.style.display = "block";

    var semiStructuredButtons = document.getElementById("semiStructuredButtons");
    semiStructuredButtons.style.display = "block";

    if(!autoPlay)
      return;

    modifyClick();

}

function acceptClick(){
  console.log("acceptClick");  

  document.getElementById('Tru-Tructor').style.visibility = 'hidden';

    var semiStructuredVideo = document.getElementById("semiStructured");
  // if(theState == State.Start) {
  //   theState = State.DirectAccept;  
  //   semiStructuredVideo.src = '../StreamingAssets/SemiStructured/DirectAccept.webm';
  //   semiStructuredVideo.onended = function(){ onDirectAcceptVideoEnd();}
  // } 
  // else if(theState == State.Modify) {
  //   theState = State.ModifyAccept;  
  //   semiStructuredVideo.src = '../StreamingAssets/SemiStructured/ModifyAccept.webm';
  //   semiStructuredVideo.onended = function(){ onModifyAcceptVideoEnd();}
  // } else {
  //   return;
  // }

  if(theState == 0) {
    theState = 1;
    semiStructuredVideo.src = '../StreamingAssets/SemiStructured/DirectAccept.webm';
    semiStructuredVideo.onended = function(){ onDirectAcceptVideoEnd();}
  } 
  else if(theState == 2) {
    theState = 3;  
    semiStructuredVideo.src = '../StreamingAssets/SemiStructured/ModifyAccept.webm';
    semiStructuredVideo.onended = function(){ onModifyAcceptVideoEnd();}
  } else {
    return;
  }

    semiStructuredVideo.play();
}

function modifyClick(){
  console.log("modifyClick");  

  document.getElementById('Tru-Tructor').style.visibility = 'visible';

  var semiStructuredVideo = document.getElementById("semiStructured");
  if(theState == 0) {
    theState = 2;  
    semiStructuredVideo.src = '../StreamingAssets/SemiStructured/Modify.webm';
    semiStructuredVideo.onended = function(){ onModifyVideoEnd();}
  }  else {
    return;
  }
    semiStructuredVideo.play();
}


function undoClick(){
  console.log("undoClick");  
    var semiStructuredVideo = document.getElementById("semiStructured");
  if(theState == 1) {
    theState = 0;  
    semiStructuredVideo.src = '../StreamingAssets/SemiStructured/DirectAcceptReversed.webm';
    semiStructuredVideo.onended = function(){ onDirectAcceptReversedVideoEnd();}
  } 
  else if(theState == 2) {
    theState = 0;  
    semiStructuredVideo.src = '../StreamingAssets/SemiStructured/ModifyReversed.webm';
    semiStructuredVideo.onended = function(){ onModifyReversedVideoEnd();}
  } 
  else if(theState == 3) {
    theState = 0;  
    semiStructuredVideo.src = '../StreamingAssets/SemiStructured/ModifyAcceptReversed.webm';
    semiStructuredVideo.onended = function(){ onModifyAcceptReversedVideoEnd();}
  }  else {
    return;
  }
    semiStructuredVideo.play();
}


function onDirectAcceptVideoEnd(){
  console.log('onDirectAcceptVideoEnd');
}

function onModifyVideoEnd(){
  console.log('onModifyVideoEnd');
  if(!autoPlay)
    return;
  acceptClick();

}


function onModifyAcceptVideoEnd(){
  console.log('onModifyAcceptVideoEnd');
  if(!autoPlay)
    return;
    window.location="../views/structured.html?autoPlay=true";
}


function onDirectAcceptReversedVideoEnd(){
  console.log('onDirectAcceptReversedVideoEnd');
}

function onModifyReversedVideoEnd(){
  console.log('onModifyReversedVideoEnd');
}

function onModifyAcceptReversedVideoEnd(){
  console.log('onModifyAcceptReversedVideoEnd');
}
