$(document).ready(function () {
  $('.ml-loader').hide();

  landing();
});


// let theState = State.Start;
let theState = 0; //0-Start, 1-DirectAccept, 2-Modify, 3-ModifyAccept
let autoPlay = false;

function landing() {

  var landingButtons = document.getElementById("landingButtons");
  landingButtons.style.display = "none";
  setTimeout(function () { landingButtons.style.display = "block"; }, 4000);
}

function documentSamplesClick() {
  // window.location="views/documentSamples.html";
  $('.home, .div').hide();
  $('.document-samples').show();

  // footer buttons
  var x = document.querySelector(".document-samples #landingButtons");
  x.style.display = "block";

}

function viewDemoClick() {
  // window.location="../views/unstructured.html";
  $('.home, .div').hide();
  unStructuredClick();
}

function unStructuredClick(source) {
  if (source === 'buttonClick') {
    autoPlay = false;
  }
  // window.location="../views/unstructured.html";
  $('.div').hide();
  $('.un-structured').show();

  var vid = document.getElementById("unStructured");
  vid.currentTime = 0;
  vid.load();
  vid.play();

  // footer buttons
  var x = document.querySelector(".un-structured #landingButtons");
  x.style.display = "block";

  var brainVideo = document.getElementById("brainVideo");
  brainVideo.style.display = "none";
  setTimeout(function () { brainVideo.style.display = "block"; }, 10000);

  if (!autoPlay)
    return;

}


function playVideoFromBeginning() {
  var vid = document.getElementById("semiStructured");
  // console.log(vid.currentTime)
  vid.currentTime = 0;
  vid.load();
  vid.play();
}

function semiStructuredClick(source) {
  // window.location="../views/semiStructured.html";
  if (source === 'buttonClick') {
    autoPlay = false;
  }
  $('.div').hide();
  $('.semi-structured').show();
  // $('#semiStructured').show();

  playVideoFromBeginning();

  // footer buttons
  var x = document.querySelector(".semi-structured #landingButtons");
  x.style.display = "block";

  var popUp = document.getElementById("popUp");
  popUp.style.display = "none";
  setTimeout(function () { popUp.style.display = "block"; popUp.play(); }, 2000);
  popUp.onended = function () {
    console.log("PopUp video has ended");
    popUp.style.display = "none";
  };

  var errorIcon = document.getElementById("errorIcon");
  errorIcon.style.display = "none";

  var semiStructuredButtons = document.getElementById("semiStructuredButtons");
  semiStructuredButtons.style.display = "none";

  // theState = State.Start;
  theState = 0;

  var semiStructuredVideo = document.getElementById("semiStructured");
  semiStructuredVideo.onended = function () { onStartVideoEnd(); }

}

function onStartVideoEnd() {
  console.log('onStartVideoEnd');
  var errorIcon = document.getElementById("errorIcon");
  errorIcon.style.display = "block";

  var semiStructuredButtons = document.getElementById("semiStructuredButtons");
  semiStructuredButtons.style.display = "block";

  if (!autoPlay)
    return;

  modifyClick();
}

function acceptClick() {
  console.log("acceptClick");

  document.getElementById('Tru-Tructor').style.visibility = 'hidden';
  var semiStructuredVideo = document.getElementById("semiStructured");
  if (theState == 0) {
    theState = 1;
    semiStructuredVideo.src = './StreamingAssets/SemiStructured/DirectAccept.webm';
    semiStructuredVideo.onended = function () { onDirectAcceptVideoEnd(); }
  }
  else if (theState == 2) {
    theState = 3;
    semiStructuredVideo.src = './StreamingAssets/SemiStructured/ModifyAccept.webm';
    semiStructuredVideo.onended = function () { onModifyAcceptVideoEnd(); }
  } else {
    return;
  }

  semiStructuredVideo.play();
}

function modifyClick() {
  console.log("modifyClick");

  document.getElementById('Tru-Tructor').style.visibility = 'visible';

  var semiStructuredVideo = document.getElementById("semiStructured");
  if (theState == 0) {
    theState = 2;
    semiStructuredVideo.src = './StreamingAssets/SemiStructured/Modify.webm';
    semiStructuredVideo.onended = function () { onModifyVideoEnd(); }
  } else {
    return;
  }
  semiStructuredVideo.play();
}

function undoClick() {
  console.log("undoClick");
  var semiStructuredVideo = document.getElementById("semiStructured");
  if (theState == 1) {
    theState = 0;
    semiStructuredVideo.src = './StreamingAssets/SemiStructured/DirectAcceptReversed.webm';
    semiStructuredVideo.onended = function () { onDirectAcceptReversedVideoEnd(); }
  }
  else if (theState == 2) {
    theState = 0;
    semiStructuredVideo.src = './StreamingAssets/SemiStructured/ModifyReversed.webm';
    semiStructuredVideo.onended = function () { onModifyReversedVideoEnd(); }
  }
  else if (theState == 3) {
    theState = 0;
    semiStructuredVideo.src = './StreamingAssets/SemiStructured/ModifyAcceptReversed.webm';
    semiStructuredVideo.onended = function () { onModifyAcceptReversedVideoEnd(); }
  } else {
    return;
  }
  semiStructuredVideo.play();
}

function onDirectAcceptVideoEnd() {
  console.log('onDirectAcceptVideoEnd');
}

function onModifyVideoEnd() {
  console.log('onModifyVideoEnd');
  if (!autoPlay)
    return;
  acceptClick();

}

function onModifyAcceptVideoEnd() {
  console.log('onModifyAcceptVideoEnd');
  if (!autoPlay)
    return;
  // window.location="../views/structured.html?autoPlay=true";
  document.getElementById("semiStructured").removeAttribute("src");
  structuredClick();
}

function onDirectAcceptReversedVideoEnd() {
  console.log('onDirectAcceptReversedVideoEnd');
}

function onModifyReversedVideoEnd() {
  console.log('onModifyReversedVideoEnd');
}

function onModifyAcceptReversedVideoEnd() {
  console.log('onModifyAcceptReversedVideoEnd');
}


function structuredClick(source) {
  // window.location="../views/structured.html";
  if (source === 'buttonClick') {
    autoPlay = false;
  }

  $('.div').hide();
  $('.structured').show();

  var vid = document.getElementById("structured");
  vid.currentTime = 0;
  vid.load();
  vid.play();

  // footer buttons
  var x = document.querySelector(".structured #landingButtons");
  x.style.display = "block";

  var brainVideo = document.getElementById("brain");
  brainVideo.style.display = "none";
  setTimeout(function () { brainVideo.style.display = "block"; }, 12000);

  if (!autoPlay)
    return;
}

function imagesClick(source) {
  // window.location="../views/images.html";
  if (source === 'buttonClick') {
    autoPlay = false;
  }

  $('.div').hide();
  $('.doc-images').show();

  var vid = document.getElementById("images");
  vid.currentTime = 0;
  vid.load();
  vid.play();

  var vid = document.getElementById("images");
  vid.currentTime = 0;

  // footer buttons
  var x = document.querySelector(".doc-images #landingButtons");
  x.style.display = "block";

  if (!autoPlay)
    return;
}


// restart and autoplay
function restartClick() {
  // window.location="../index.html";
  $('.div').hide();
  $('.home').show();

  var vid = document.querySelector(".home .bgVideo");
  vid.currentTime = 0;
  var vid1 = document.getElementById("logoVideo");
  vid1.currentTime = 0;

  landing();
}


function autoplayClick() {
  autoPlay = true;

  if (autoPlay) {
    unStructuredClick();

    var unStructuredVideo = document.getElementById("unStructured");
    unStructuredVideo.onended = function () {
      if (!autoPlay) return;
      semiStructuredClick();
    }

    var semiStructuredVideo = document.getElementById("semiStructured");
    semiStructuredVideo.onended = function () {
      if (!autoPlay) return;
      structuredClick();
    }

    var structuredVideo = document.getElementById("structured");
    structuredVideo.onended = function () {
      if (!autoPlay) return;
      imagesClick();
    }

    var imagesVideo = document.getElementById("images");
    imagesVideo.onended = function () {
      if (!autoPlay) return;
      unStructuredClick();
    }
  }
}