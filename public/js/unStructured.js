 const queryString = window.location.search;

 const urlParams = new URLSearchParams(queryString);
 const autoPlay = urlParams.get('autoPlay');


 $(document).ready(function(){
  $('.ml-loader').hide();
  //  console.log('Ready');
   var brainVideo = document.getElementById("brainVideo");
  	brainVideo.style.display = "none";
   setTimeout(function() { brainVideo.style.display = "block"; }, 10000);

   if(!autoPlay)
      return;

    var unStructuredVideo = document.getElementById("unStructured");
    unStructuredVideo.onended = function(){ onVideoEnd();}


});


function onVideoEnd(){
  console.log('onVideoEnd');
    window.location="../views/semiStructured.html?autoPlay=true";

}
