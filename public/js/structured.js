const queryString = window.location.search;

const urlParams = new URLSearchParams(queryString);
const autoPlay = urlParams.get('autoPlay');

 $(document).ready(function(){

  $('.ml-loader').hide();
  //  console.log('Ready');
  
   // var x = document.getElementById("landingButtons");
   // x.style.display = "block";
   var brainVideo = document.getElementById("brain");
  	brainVideo.style.display = "none";
   setTimeout(function() { brainVideo.style.display = "block"; }, 12000);

    if(!autoPlay)
      return;

    var structuredVideo = document.getElementById("structured");
    structuredVideo.onended = function(){ onVideoEnd();}


});


function onVideoEnd(){
  console.log('onVideoEnd');
    window.location="../views/images.html?autoPlay=true";

}
